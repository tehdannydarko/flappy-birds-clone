package com.tehdannydarko;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.IOException;

public class Bird implements Updateable, Renderable {

    private float x;
    private float y;

    private float yVelocity;
    private float baseYVelocity = -6.0f;

    private float gravity = 0.25f;

    private Pipes pipes;
    private int scoredPipe = 0;

    private int score = 0;

    private Font gameFont = new Font("Arial", Font.BOLD, 30);

    private BufferedImage flapUp;
    private BufferedImage flapDown;

    public Bird(Pipes pipes) {
        resetBird();
        this.pipes = pipes;

        try {
            this.flapUp = Sprite.getSprite("bird_up.png");
            this.flapDown = Sprite.getSprite("bird_down.png");
        } catch (final IOException e) {
            System.err.print(e.getMessage());
            System.exit(1);
        }
    }

    public void resetBird() {
        this.x = 100;
        this.y = 100;
        this.yVelocity = this.baseYVelocity;
    }

    private void flap() {
        this.yVelocity = this.baseYVelocity;
    }

    @Override
    public void render(Graphics2D g, float interpolation) {
        g.setColor(Color.RED);

        g.drawImage(
                (this.yVelocity <= 0) ? this.flapUp : this.flapDown,
                (int) this.x,
                (int) (this.y + (this.yVelocity * interpolation)),
                null);

        g.setFont(this.gameFont);
        g.drawString("Score: " + this.score, 20, 50);
    }

    @Override
    public void update(Input input) {
        this.y += this.yVelocity;
        this.yVelocity += this.gravity;

        if (this.y < 0) {
            this.y = 0;
            this.yVelocity = 0;
        }

        if (input.isSpacePressed()) {
            flap();
        }

        final float[] pipeCoords = this.pipes.getCurrentPipe();
        final float pipeX = pipeCoords[0];
        final float pipeY = pipeCoords[1];

        if((this.x > pipeX)
                && (this.x <= pipeX + this.pipes.getPipeWidth())
                && (this.y <= pipeY
                    || this.y >= pipeY + this.pipes.getPipeVerticalSpacing()
                    || this.y > Game.HEIGHT)) {
            this.pipes.resetPipes();
            resetBird();
            this.score = 0;
        } else {
            int currentPipeId = this.pipes.getCurrentPipeId();
            this.score = (this.scoredPipe != currentPipeId) ? score + 1 : score;
            this.scoredPipe = currentPipeId;
        }
    }
}
